export { TranslateModule, TranslateParser, TranslatePipe, TranslateService } from '@ngx-translate/core';
export { PapyrusPaginatorIntl } from './src/lib/classes/papyrusPaginatorIntl';
export { LanguageSelectorComponent } from './src/lib/components/language-selector/language-selector.component';
export { LanguageSelectorModule } from './src/lib/components/language-selector/language-selector.module';
export { PapyrusClientModule, PapyrusInitFactory } from './src/lib/papyrus-client.module';
