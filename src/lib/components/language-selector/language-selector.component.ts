import { Component, Inject, InjectionToken, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

export const LANGUAGES = new InjectionToken<string[] | undefined>('PAPYRUS_LANGUAGES');

@Component({
	selector: 'lib-language-selector',
	templateUrl: './language-selector.component.html',
	styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent {
	@Input('useIcon') useIcon = false;

	constructor(public translateService: TranslateService, @Inject(LANGUAGES) public languages: string[] | undefined) {}

	public selectLanguage(language: string) {
		this.translateService.use(language);
		localStorage.setItem('language', language);
	}
}
