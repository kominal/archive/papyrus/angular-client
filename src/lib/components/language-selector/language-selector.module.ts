import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { LanguageSelectorComponent } from './language-selector.component';

@NgModule({
	declarations: [LanguageSelectorComponent],
	imports: [CommonModule, TranslateModule, MatMenuModule, MatIconModule, MatButtonModule, MatTooltipModule],
	exports: [LanguageSelectorComponent],
})
export class LanguageSelectorModule {}
