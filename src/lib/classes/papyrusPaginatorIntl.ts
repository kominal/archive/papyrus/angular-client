import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateParser, TranslateService } from '@ngx-translate/core';

export class PapyrusPaginatorIntl extends MatPaginatorIntl {
	private rangeLabelIntl: string = '';

	constructor(private translateService: TranslateService, private translateParser: TranslateParser) {
		super();
		this.translateService
			.get(['transl.pagination.itemsPerPage', 'transl.pagination.previousPage', 'transl.pagination.nextPage', 'transl.pagination.range'])
			.subscribe((translation) => {
				this.itemsPerPageLabel = translation['transl.pagination.itemsPerPage'];
				this.nextPageLabel = translation['transl.pagination.nextPage'];
				this.previousPageLabel = translation['transl.pagination.previousPage'];
				this.rangeLabelIntl = translation['transl.pagination.range'];
				this.changes.next();
			});
	}

	getRangeLabel = (page: number, pageSize: number, length: number) => {
		length = Math.max(length, 0);
		const startIndex = length > 0 ? page * pageSize + 1 : 0;
		const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
		return this.translateParser.interpolate(this.rangeLabelIntl, { startIndex, endIndex, length });
	};
}
